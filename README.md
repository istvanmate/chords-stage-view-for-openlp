This stage view was initially created here: https://forums.openlp.org/discussion/2544/chords-in-openlp

At some point it was included in OpenLP. Unfortunately, at that time I did not know Python nor git to contribute in the proper way, so most of current features were developed offline. Now I know a little git and I can logic some Python code together to get it working. Ultimate goal is to implement all features - that would proove useful - into OpenLP.

Current features:
- chord support with proper chord notation
- [A>] will show chord before next character, not on it
- per song element (verse, chorus, etc.) transpose functionality with song key and key modulation support
- not repeated bass notes only mode
- mid song notes support (can act as a MD for musicians), requires custom formatting tag: {notes}=`<span class="notes" style="display:none">` {/notes}=`</span>`
- score sheet support, requires custom formatting tag: {score}=`<div id="sheet" style="display:none">` {/score}=`</div>`, for {score}songname{/score} songname-1.svg, songname-2.svg, etc. should be added to /scores/ directory, one image file per page. We use musescore to create scores and export them to .svg for best "resolution". Changing .svg to .png or .jpg in stage.js will allow pictures to be added as scores. Current setting is custom tailored to A4 portrait aspect ratio. Page advancing is optimized for touch devices, done with right->left, left->right swipe
- builtin metronome, following tags are supported anywhere in the song (we use it inside {notes}{/notes}), will accent every 2/4, 3/8..., beats on eight notes too for tempo slower than 115: Tempo=80, Tick=3 (for 3/4, 6/8...), BpB=2 (beats per 2/4, 2 will skip 8th notes), Swing=66; mid song tempo change will result in a tempo list, with button to change to the next tempo
- mark words or even part of words with red for getting attention, requires custom formatting tag: {warn}=`<span class="worshipbandwarning">` {/warn}=`</span>`
- {cnl} is a stageview only linebreak, if line is overflowing uncontrollably because of chords
- current slide is part of the whole song and it is being scrolled to. It allows last slide's bottom part to be visible after slide advance, page is manually scrollable
- includes nosleep code from git.io/vfn01 trigger is rigged to document object, can be turned off with the light bulb button
- semi-offline usage, after pressing the play button any ws update is being ignored
- semi-offline mode search functionality will search song titles in song db on keypress, on pressing second search button search is performed in the whole songs. Selecting the song will perform a send to live.
- used font is not included (Arial Narrow is a MS font, modified), stage.css should be modified for different font usage

The following custom formatting tags are supported(/required):
```html
{notes}  = <span class="notes" style="display:none">
{/notes} = </span>
{warn}   = <span class="worshipbandwarning">
{/warn}  = </span>
{cnl}    = <span class="chordnewline" style="display:none"></span>
```
Optionally
```html
{score}      = <div id="sheet" style="display:none">
{/score}     = </div>
{musescore}  = <div id="musescore" style="display:none">
{/musescore} = </div>
```
Set Hide content from Live/Preview as desired