const cacheName = 'v1.0001'

const cacheAssets = [
    '/stage/worship/',
    '/stage/worship/stage.html',
    '/stage/worship/stage.css',
    '/stage/worship/stage.js',
    '/stage/worship/service-worker.js',
    '/stage/worship/assets/ARIALN.TTF',
    '/stage/worship/assets/ARIALNB.TTF',
    '/stage/worship/assets/jquery-3.6.0.min.js',
    '/stage/worship/assets/jquery.js',
    '/stage/worship/assets/jquery.min.js',
    '/stage/worship/assets/jquery.mobile.js',
    '/stage/worship/assets/jquery.mobile.min.css',
    '/stage/worship/assets/jquery.mobile.min.js',
    '/stage/worship/images/ajax-loader.gif',
    '/stage/worship/images/ajax-loader.png',
    '/stage/worship/images/favicon.ico',
    '/stage/worship/images/form-check-off.png',
    '/stage/worship/images/form-check-on.png',
    '/stage/worship/images/form-radio-off.png',
    '/stage/worship/images/form-radio-on.png',
    '/stage/worship/images/icons-18-black.png',
    '/stage/worship/images/icons-18-white.png',
    '/stage/worship/images/icons-36-black.png',
    '/stage/worship/images/icons-36-white.png',
    '/stage/worship/images/icon-search-black.png',
    '/stage/worship/images/openlp.png',
    '/stage/worship/images/openlp-logo.svg',
    '/stage/worship/images/ui-icon-blank.png',
    '/stage/worship/images/ui-icon-blank-180.png',
    '/stage/worship/images/ui-icon-unblank.png',
    '/stage/worship/images/ui-icon-unblank-180.png'
]

self.addEventListener('install', e => {
    console.log('Service worker installed.')
    e.waitUntil(
        caches
            .open(cacheName)
            .then(cache => {
                console.log('Service worker caching files:')
                cache.addAll(cacheAssets)
            })
            .then(() => self.skipWaiting())
    )
})

self.addEventListener('activate', (e) => {
    console.log('Service worker activated.')
    e.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cache => {
                    if (cache !== cacheName) {
                        console.log('Service worker: Clearing old cache')
                        return caches.delete(cache)
                    }
                })
            )
        })
    )
})

self.addEventListener('fetch', e => {
    console.log('Service worker: Fetching')
    e.respondWith(
        fetch(e.request).catch(() => caches.match(e.request))
    )
})