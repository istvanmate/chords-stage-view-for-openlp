/******************************************************************************
 * OpenLP - Open Source Lyrics Projection                                      *
 * --------------------------------------------------------------------------- *
 * Copyright (c) 2008-2014 Raoul Snyman                                        *
 * Portions copyright (c) 2008-2014 Tim Bentley, Gerald Britton, Jonathan      *
 * Corwin, Samuel Findlay, Michael Gorven, Scott Guerrieri, Matthias Hub,      *
 * Meinert Jordan, Armin Köhler, Erik Lundin, Edwin Lunando, Brian T. Meyer.   *
 * Joshua Miller, Stevan Pettit, Andreas Preikschat, Mattias Põldaru,          *
 * Christian Richter, Philip Ridout, Simon Scudder, Jeffrey Smith,             *
 * Maikel Stuivenberg, Martin Thompson, Jon Tibble, Dave Warnock,              *
 * Frode Woldsund, Martin Zibricky                                             *
 * Added chord formating: Tomas                                             *
 * --------------------------------------------------------------------------- *
 * This program is free software; you can redistribute it and/or modify it     *
 * under the terms of the GNU General Public License as published by the Free  *
 * Software Foundation; version 2 of the License.                              *
 *                                                                             *
 * This program is distributed in the hope that it will be useful, but WITHOUT *
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    *
 * more details.                                                               *
 *                                                                             *
 * You should have received a copy of the GNU General Public License along     *
 * with this program; if not, write to the Free Software Foundation, Inc., 59  *
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA                          *
 ******************************************************************************/

/*serviceWorker = null
function register_service_worker() {
	if ('serviceWorker' in navigator) {
		window.addEventListener('load', () => {
			navigator.serviceWorker
				.register('./service-worker.js')
				.then(reg => {
					serviceWorker = reg
					console.log('Service worker registered.')
				})
				.catch(err => console.log(`Service worker error: ${err}`))
		})
	}
}

function unregister_service_worker() {
	window.navigator.serviceWorker.getRegistrations()
		.then(registrations => {
			registrations.forEach(registration => {
				registration.unregister()
				console.log('Service worker unregistered.')
			})
		})
		.catch(err => {
			console.log('Service worker unregister error!')
		})
}
unregister_service_worker()*/

String.prototype.insert = function(index, string) {
	if (index > 0) {
		return this.substring(0, index) + string + this.substring(index)
	}
	return string + this
}

let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);

var currentChordKey, slideCounter, lastChord, lastBass, debug = false, sheetmusicname, sheetpage, bassmode = false, lastTransposedChord = "", newline = true, textInSlide = false, useSpecialNotationChars = true

function transposeChord(chord, transposeValue, currentChordKey, ignoreBassMode = false, useNotation = false) {
	if (debug) console.log("transposechord() ->")
	var chordToLeft = false
	if (chord.indexOf('&gt;') > -1) {
		chordToLeft = true
		chord = chord.replace('&gt;', '')
	}
	var chordSplit = chord.replace(/♭/g, 'b').replace(/Ø/g,'0').replace(/♯/g,"#").replace(/×/g,"x").split('/'), transposedChord = '', note, notenumber, rest, currentChord,
	notesSharp =          ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'H'],
	notesFlat =           ['C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab', 'A', 'B', 'H'],
	notesPreferred =      ['-', 'b', '#', 'b', '#', 'b', '#', '#', 'b', '#', 'b', '#'],
	notesPreferredMinor = ['b', '#', 'b', 'b', 'b', 'b', '#', 'b', '#', 'b', 'b', '#'],
	scales = {
		'C'    : ['C',   'Db', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'Ab', 'A',   'B',  'H'],
		'Am'   : ['C',   'Db', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'Ab', 'A',   'B',  'H'],
		'C#'   : ['H#',  'C#', 'D',   'D#', 'E',  'E#', 'F#', 'G',   'G#', 'A',   'A#', 'H'],
		'A#m'  : ['H#',  'C#', 'D',   'D#', 'E',  'E#', 'F#', 'G',   'G#', 'A',   'A#', 'H'],
		'Db'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',  'Gb', 'Abb', 'Ab', 'Bb',  'B',  'Cb'],
		'Bm'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',  'Gb', 'Abb', 'Ab', 'Bb',  'B',  'Cb'],
		'D'    : ['C',   'C#', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'Ab', 'A',   'B',  'H'],
		'Hm'   : ['C',   'C#', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'Ab', 'A',   'B',  'H'],
		'D#'   : ['H#',  'C#', 'Cx',  'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'A',   'A#', 'H'],
		'H#m'  : ['H#',  'C#', 'Cx',  'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'A',   'A#', 'H'],
		'Eb'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',  'Gb', 'G',   'Ab', 'Bb',  'B',  'Cb'],
		'Cm'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',  'Gb', 'G',   'Ab', 'Bb',  'B',  'Cb'],
		'E'    : ['C',   'C#', 'D',   'D#', 'E',  'F',  'F#', 'G',   'G#', 'A',   'B',  'H'],
		'C#m'  : ['C',   'C#', 'D',   'D#', 'E',  'F',  'F#', 'G',   'G#', 'A',   'B',  'H'],
		'F'    : ['C',   'Db', 'D',   'Eb', 'E',  'F',  'Gb', 'G',   'Ab', 'A',   'B',  'Cb'],
		'Dm'   : ['C',   'C#', 'D',   'Eb', 'E',  'F',  'Gb', 'G',   'Ab', 'A',   'B',  'H'],
		'F#'   : ['C',   'C#', 'D',   'D#', 'E',  'E#', 'F#', 'G',   'G#', 'A',   'A#', 'H'],
		'D#m'  : ['C',   'C#', 'D',   'D#', 'E',  'E#', 'F#', 'G',   'G#', 'A',   'A#', 'H'],
		'Gb'   : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'F',  'Gb', 'Abb', 'Ab', 'Bb',  'B',  'Cb'],
		'Ebm'  : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'F',  'Gb', 'Abb', 'Ab', 'Bb',  'B',  'Cb'],
		'G'    : ['C',   'Db', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'Ab', 'A',   'B',  'H'],
		'Em'   : ['C',   'Db', 'D',   'D#', 'E',  'F',  'F#', 'G',   'Ab', 'A',   'B',  'H'],
		'G#'   : ['H#',  'C#', 'D',   'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'A',   'A#', 'H'],
		'E#m'  : ['H#',  'C#', 'D',   'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'A',   'A#', 'H'],
		'Ab'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',  'Gb', 'G',   'Ab', 'Bb',  'B',  'Cb'],
		'Fm'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',  'Gb', 'G',   'Ab', 'Bb',  'B',  'Cb'],
		'A'    : ['C',   'C#', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'G#', 'A',   'B',  'H'],
		'F#m'  : ['C',   'C#', 'D',   'Eb', 'E',  'F',  'F#', 'G',   'G#', 'A',   'B',  'H'],
		'A#'   : ['H#',  'C#', 'Cx',  'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'Gx',  'A#', 'H'],
		'F##m' : ['H#',  'C#', 'Cx',  'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'Gx',  'A#', 'H'],
		'Fxm'  : ['H#',  'C#', 'Cx',  'D#', 'E',  'E#', 'F#', 'Fx',  'G#', 'Gx',  'A#', 'H'],
		'B'    : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',  'Gb', 'G',   'Ab', 'A',   'B',  'Cb'],
		'Gm'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',  'Gb', 'G',   'Ab', 'A',   'B',  'Cb'],
		'H'    : ['C',   'C#', 'D',   'D#', 'E',  'F',  'F#', 'G',   'G#', 'A',   'A#', 'H'],
		'G#m'  : ['C',   'C#', 'D',   'D#', 'E',  'F',  'F#', 'G',   'G#', 'A',   'A#', 'H'],
		'Cb'   : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'Gbb', 'Gb', 'Abb', 'Ab', 'Bb', 'B', 'Cb'],
		'Abm'  : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'Gbb', 'Gb', 'Abb', 'Ab', 'Bb', 'B', 'Cb']
	},
	scalesInternational = {
		'C'    : ['C',   'Db', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'Ab', 'A',   'Bb', 'B'],
		'Am'   : ['C',   'Db', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'Ab', 'A',   'Bb', 'B'],
		'C#'   : ['B#',  'C#', 'D',   'D#', 'E',  'E#',  'F#', 'G',   'G#', 'A',   'A#', 'B'],
		'A#m'  : ['B#',  'C#', 'D',   'D#', 'E',  'E#',  'F#', 'G',   'G#', 'A',   'A#', 'B'],
		'Db'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',   'Gb', 'Abb', 'Ab', 'Bbb', 'Bb', 'Cb'],
		'Bbm'  : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',   'Gb', 'Abb', 'Ab', 'Bbb', 'Bb', 'Cb'],
		'D'    : ['C',   'C#', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'Ab', 'A',   'Bb', 'B'],
		'Bm'   : ['C',   'C#', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'Ab', 'A',   'Bb', 'B'],
		'D#'   : ['B#',  'C#', 'Cx',  'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'A',   'A#', 'B'],
		'B#m'  : ['B#',  'C#', 'Cx',  'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'A',   'A#', 'B'],
		'Eb'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',   'Gb', 'G',   'Ab', 'Bbb', 'Bb', 'Cb'],
		'Cm'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',   'Gb', 'G',   'Ab', 'Bbb', 'Bb', 'Cb'],
		'E'    : ['C',   'C#', 'D',   'D#', 'E',  'F',   'F#', 'G',   'G#', 'A',   'Bb', 'B'],
		'C#m'  : ['C',   'C#', 'D',   'D#', 'E',  'F',   'F#', 'G',   'G#', 'A',   'Bb', 'B'],
		'F'    : ['C',   'Db', 'D',   'Eb', 'E',  'F',   'Gb', 'G',   'Ab', 'A',   'Bb', 'Cb'],
		'Dm'   : ['C',   'C#', 'D',   'Eb', 'E',  'F',   'Gb', 'G',   'Ab', 'A',   'Bb', 'B'],
		'F#'   : ['C',   'C#', 'D',   'D#', 'E',  'E#',  'F#', 'G',   'G#', 'A',   'A#', 'B'],
		'D#m'  : ['C',   'C#', 'D',   'D#', 'E',  'E#',  'F#', 'G',   'G#', 'A',   'A#', 'B'],
		'Gb'   : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'F',   'Gb', 'Abb', 'Ab', 'Bbb', 'Bb', 'Cb'],
		'Ebm'  : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'F',   'Gb', 'Abb', 'Ab', 'Bbb', 'Bb', 'Cb'],
		'G'    : ['C',   'Db', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'Ab', 'A',   'Bb', 'B'],
		'Em'   : ['C',   'Db', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'Ab', 'A',   'Bb', 'B'],
		'G#'   : ['B#',  'C#', 'D',   'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'A',   'A#', 'B'],
		'E#m'  : ['B#',  'C#', 'D',   'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'A',   'A#', 'B'],
		'Ab'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',   'Gb', 'G',   'Ab', 'Bbb', 'Bb', 'Cb'],
		'Fm'   : ['C',   'Db', 'Ebb', 'Eb', 'Fb', 'F',   'Gb', 'G',   'Ab', 'Bbb', 'Bb', 'Cb'],
		'A'    : ['C',   'C#', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'G#', 'A',   'Bb', 'B'],
		'F#m'  : ['C',   'C#', 'D',   'Eb', 'E',  'F',   'F#', 'G',   'G#', 'A',   'Bb', 'B'],
		'A#'   : ['B#',  'C#', 'Cx',  'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'Gx',  'A#', 'B'],
		'F##m' : ['B#',  'C#', 'Cx',  'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'Gx',  'A#', 'B'],
		'Fxm'  : ['B#',  'C#', 'Cx',  'D#', 'E',  'E#',  'F#', 'Fx',  'G#', 'Gx',  'A#', 'B'],
		'Bb'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',   'Gb', 'G',   'Ab', 'A',   'Bb', 'Cb'],
		'Gm'   : ['C',   'Db', 'D',   'Eb', 'Fb', 'F',   'Gb', 'G',   'Ab', 'A',   'Bb', 'Cb'],
		'B'    : ['C',   'C#', 'D',   'D#', 'E',  'F',   'F#', 'G',   'G#', 'A',   'A#', 'B'],
		'G#m'  : ['C',   'C#', 'D',   'D#', 'E',  'F',   'F#', 'G',   'G#', 'A',   'A#', 'B'],
		'Cb'   : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'Gbb', 'Gb', 'Abb', 'Ab', 'Bbb', 'Bb', 'Cb'],
		'Abm'  : ['Dbb', 'Db', 'Ebb', 'Eb', 'Fb', 'Gbb', 'Gb', 'Abb', 'Ab', 'Bbb', 'Bb', 'Cb']
	},
	noteNumbers = {
		'C'  :  0, 'H#'  :  0, 'B##' :  0, 'Bx'  :  0, 'Dbb' :  0,
		'C#' :  1, 'Db'  :  1,
		'D'  :  2, 'C##' :  2, 'Cx'  :  2, 'Ebb' :  2,
		'D#' :  3, 'Eb'  :  3,
		'E'  :  4, 'D##' :  4, 'Dx'  :  4, 'Fb'  :  4,
		'F'  :  5, 'E#'  :  5, 'Gbb' :  5,
		'F#' :  6, 'Gb'  :  6,
		'G'  :  7, 'F##' :  7, 'Fx'  :  7, 'Abb' :  7,
		'G#' :  8, 'Ab'  :  8,
		'A'  :  9, 'G##' :  9, 'Gx'  :  9, 'Bb'  :  9, 'Hbb' :  9,
		'B'  : 10, 'A#'  : 10, 'Hb'  : 10,
		'H'  : 11, 'B#'  : 11, 'A##' : 11, 'Ax'  : 11, 'Cb'  : 11
	},
	noteNumbersInternational = {
		'C'  :  0, 'B#'  :  0, 'Dbb' :  0,
		'C#' :  1, 'Db'  :  1, 'B##' :  1, 'Bx'  :  1,
		'D'  :  2, 'C##' :  2, 'Cx'  :  2, 'Ebb' :  2,
		'D#' :  3, 'Eb'  :  3,
		'E'  :  4, 'D##' :  4, 'Dx'  :  4, 'Fb'  :  4,
		'F'  :  5, 'E#'  :  5, 'Gbb' :  5,
		'F#' :  6, 'Gb'  :  6,
		'G'  :  7, 'F##' :  7, 'Fx'  :  7, 'Abb' :  7,
		'G#' :  8, 'Ab'  :  8,
		'A'  :  9, 'G##' :  9, 'Gx'  :  9, 'Bbb' :  9,
		'Bb' : 10, 'A#'  : 10,
		'B'  : 11, 'A##' : 11, 'Ax'  : 11, 'Cb'  : 11
	}
	for (i = 0; i < chordSplit.length; i++) {
		transposedChord += ((i > 0) && !bassmode) ? '/' : ""
		currentChord = chordSplit[i]
		if (currentChord.charAt(0) === '(') {
			transposedChord += '('
			if (currentChord.length > 1) {
				currentChord = currentChord.substr(1)
			} else {
				currentChord = ""
			}
		}
		if (currentChord.length > 0) {
			if (currentChord.length > 1) {
				if ('#bx'.indexOf(currentChord.charAt(1)) === -1) {
					note = currentChord.substr(0, 1)
					rest = currentChord.substr(1)
				} else {
					if ('#bx'.indexOf(currentChord.charAt(2)) === -1) {
						note = currentChord.substr(0, 2)
						rest = currentChord.substr(2)
					} else {
						note = currentChord.substr(0, 3)
						rest = currentChord.substr(3)
					}
				}
				minor = rest.substr(0, 1).toLowerCase() === 'm' && rest.substr(0, 3).toLowerCase() !== 'maj' ? 'm' : ''
				if (minor) {rest = rest.substr(1)}
			} else {
				note = currentChord
				minor = ''
				rest = ''
			}
			if (noteNumbers[note.charAt(0).toUpperCase() + note.substr(1)] === undefined) {
				if (debug) {console.log("transposeChord: [" + chord + "] -> [" + chord + "]; currentChordKey=" + currentChordKey)}
				if (debug) console.log(`if (chordToLeft (${chordToLeft})) chord (${chord}) += '&gt;'`)
				if (chordToLeft) chord += '&gt;'
				if (debug) console.log("transposechord() <- return chord (" + chord + ")")
				return chord
			}
			notenumber = noteNumbers[note.charAt(0).toUpperCase() + note.substr(1)]
			notenumber += parseInt(transposeValue)
			while (notenumber > 11) {notenumber -= 12}
			while (notenumber < 0) {notenumber += 12}
			if (i === 0) {
				if (currentChordKey) {
					lastChord = scales[currentChordKey][notenumber]
				} else {
					if (minor) {
						if (notesPreferredMinor[notenumber] === '#') {
							lastChord = notesSharp[notenumber]
						} else if (notesPreferredMinor[notenumber] === 'b') {
							lastChord = notesFlat[notenumber]
						} else {
							lastChord = notesSharp[notenumber]
						}
					} else {
						if (notesPreferred[notenumber] === '#') {
							lastChord = notesSharp[notenumber]
						} else if (notesPreferred[notenumber] === 'b') {
							lastChord = notesFlat[notenumber]
						} else {
							lastChord = notesSharp[notenumber]
						}
					}
				}
				transposedChord += (bassmode) ? (chordSplit.length > 1 ? "" : (lastChord.charAt(0) + (useNotation ? lastChord.substr(1).replace(/b/g, '♭').replace(/0/g,'Ø').replace(/#/g,"♯").replace(/x/g,"×") : lastChord.substr(1)))) : ((note.charAt(0) === note.charAt(0).toLowerCase() ? lastChord.charAt(0).toLowerCase() : lastChord.charAt(0)) + (useNotation ? lastChord.substr(1).replace(/b/g, '♭').replace(/0/g,'Ø').replace(/#/g,"♯").replace(/x/g,"×") : lastChord.substr(1)) + minor)
			} else {
				lastBass = scales[lastChord + minor] ? scales[lastChord + minor][notenumber] : scales[currentChordKey][notenumber]
				transposedChord += (note.charAt(0) === note.charAt(0).toLowerCase() ? lastBass.charAt(0).toLowerCase() : lastBass.charAt(0)) + (useNotation ? lastBass.substr(1).replace(/b/g, '♭').replace(/0/g,'Ø').replace(/#/g,"♯").replace(/x/g,"×") : lastBass.substr(1))
			}
			transposedChord += (!bassmode) ? (useNotation ? rest.replace(/b/g, '♭').replace(/0/g,'Ø').replace(/#/g,"♯").replace(/x/g,"×") : rest) : ""
		}
	}
	if (!bassmode || ignoreBassMode) {
		if (debug) {console.log("transposeChord: [" + chord + "] -> [" + transposedChord + "]; currentChordKey=" + currentChordKey)}
		if (debug) console.log(`if (chordToLeft (${chordToLeft})) chord (${transposedChord}) += '&gt;'`)
		if (chordToLeft) transposedChord += '&gt;'
		if (debug) console.log("transposechord() <- return transposedChord (" + transposedChord + ")")
		return transposedChord
	} else {
		if ((transposedChord === lastTransposedChord) && !newline) {
			if (debug) {console.log("transposeChord: [" + chord + "] -> []; currentChordKey=" + currentChordKey)}
			if (debug) console.log("transposechord() <- return \"\"")
			return ""
		} else {
			lastTransposedChord = transposedChord
			if (debug) {console.log("transposeChord: [" + chord + "] -> [" + transposedChord + "]; currentChordKey=" + currentChordKey)}
			if (debug) console.log("transposechord() <- return transposedChord (" + transposedChord + ")")
			if (debug) console.log(`if (chordToLeft (${chordToLeft})) chord (${transposedChord}) += '&gt;'`)
			if (chordToLeft) transposedChord += '&gt;'
			return transposedChord
		}
	}
}

window.OpenLP = {
	counter: 0,
	loadService: function (state) {
		if (debug) console.log(`-> OpenLP.loadService()`)
		if ((OpenLP.currentService != state.service) || (OpenLP.currentItem != state.item)) {
			if (debug) console.log(`OpenLP.currentService (${OpenLP.currentService}) != state.service (${state.service})`)
			OpenLP.currentService = state.service
			$.getJSON(
				"/api/v2/service/items",
				function (data) {
					OpenLP.prevSong = ""
					OpenLP.nextSong = ""
					//$("#notes").html("")
					var selected = -1
					for (var i = 0; i < data.length; i++) {
						if (data[i]["selected"]) {
							$("#songtitle").html(data[i]["title"].replace(/\n/g, "<br />"))
							selected = i
						} else {
							if (selected > -1) {
								OpenLP.nextSong += `<div id="${data[i]["id"]}" class="servicelist" onclick="OpenLP.setServiceJumpId('${data[i]["id"]}')">${data[i]["title"].replace(" Magyar Újfordítású Revideált, Közkincs; Ingyen terjeszthető","")}</div>`
							} else {
								OpenLP.prevSong += `<div id="${data[i]["id"]}" class="servicelist" onclick="OpenLP.setServiceJumpId('${data[i]["id"]}')">${data[i]["title"].replace(" Magyar Újfordítású Revideált, Közkincs; Ingyen terjeszthető","")}</div>`//'<div class="servicelist"><span class="listtitle">' + data[i]["title"].replace(" Magyar Újfordítású Revideált, Közkincs; Ingyen terjeszthető","") + `</span><span id="${data[i]["id"]}" class="button jumpbutton${(OpenLP.showRemoteControls) ? "":" hidden"}" onclick="OpenLP.setServiceJumpId('${data[i]["id"]}')">➡️</span></div>`
							}
						}
					}
					$("#prevsong").html(OpenLP.prevSong)
					$("#nextsong").html(OpenLP.nextSong)
					OpenLP.selectedService = data.length > selected + 1 ? data[selected + 1]["id"] : ""
					OpenLP.loadSlides(state)
				}
			)
		} else {
			OpenLP.loadSlides(state)
		}
		if (debug) console.log(`<- OpenLP.loadService()`)
	},
	loadSlides: function (state) {
		if (debug) console.log(`-> OpenLP.loadSlides()`)
		if (OpenLP.currentItem != state.item) {
			if (debug) console.log(`OpenLP.currentItem (${OpenLP.currentItem}) != state.item (${state.item})`)
			OpenLP.currentItem = state.item
			if (OpenLP.currentItemOld != OpenLP.currentItem) {
				OpenLP.currentItemOld = OpenLP.currentItem
				$("#sheetbutton").hide()
				$("#kotta").hide()
				OpenLP.sheetmusicname = ""
				document.querySelector(':root').style.setProperty("--vw","1vw")
				i = 1
				while ($("#kottapage" + i).length) {
					$("#kottapage" + i).remove()
					i++
				}
			}
			currentChordKey = undefined
			//chordKeyVersion = -1
			$.getJSON(
				"/api/v2/controller/live-items",
				function (data) {
					OpenLP.currentSlides = data.slides
					OpenLP.currentItemTitle = data.title
					OpenLP.currentName = data.name
					if (data.name == "songs") {
						if (localStorage.getItem(OpenLP.currentItemTitle + '_transposeValue')) {
							localStorage.setItem(OpenLP.currentItemTitle + '_transposeValue[0]',localStorage.getItem(OpenLP.currentItemTitle + '_transposeValue'))
							localStorage.removeItem(OpenLP.currentItemTitle + '_transposeValue')
						}
					}
					if (data["notes"]) {
						if ((data["notes"].substring(0,5).toLowerCase() == "timer") || (data["notes"].substring(0,10).toLowerCase() == "starttimer")) {
							if (debug) {console.log(data["notes"].split(" ")[1])}
							if (data["notes"].substring(0,10).toLowerCase() == "starttimer") {
								OpenLP.notesTimerType = "multi"
							} else {
								OpenLP.notesTimerType = "single"
							}
							//$("#notes").html(data["notes"].split(" ")[1].replace(/\n/g, "<br />"))
							OpenLP.notesTimerValue = OpenLP.convertStringToSeconds(data["notes"].split(" ")[1])
							OpenLP.notesTimerStartTime = Date.now()
							if (typeof notesCountDownTimerInterval != "undefined") clearInterval(notesCountDownTimerInterval)
							notesCountDownTimerInterval = setInterval("OpenLP.notesCountDownTimer()",1000)
						} else if ((data["notes"].substring(0,9).toLowerCase() == "stoptimer") || (OpenLP.notesTimerType == "single")) {
							if (data["notes"].substring(0,9).toLowerCase() == "stoptimer") {
								$("#notes").html(data["notes"].substring(9).replace(/\n/g, "<br />"))
							} else {
								$("#notes").html(data["notes"].replace(/\n/g, "<br />"))
							}
							if (typeof notesCountDownTimerInterval != "undefined") clearInterval(notesCountDownTimerInterval)
						}
					}
					OpenLP.currentSlide = state.slide
					OpenLP.updateSlide(true)
				}
			)
		} else {
			if (OpenLP.currentSlide != state.slide) {
				if (debug) console.log(`OpenLP.currentSlide (${OpenLP.currentSlide}) != state.slide (${state.slide})`)
				OpenLP.currentSlide = state.slide
				OpenLP.updateCurrentSlide(true)
			}
		}
		if (debug) console.log(`<- OpenLP.loadSlides()`)
	},
	updateSlide: function(scrollToCurrentSlide=true) {
		if (OpenLP.currentSlides && OpenLP.currentSlides.length > 0) {
			//start processing #prevslide
			lastfiller = "&nbsp"
			$("#slides").html(OpenLP.renderSlideHTMLElements())
			/*$("#prevsong").html(OpenLP.prevSong)
			$("#nextsong").html(OpenLP.nextSong)*/
			OpenLP.setServiceJumpId(OpenLP.selectedService)
			if(OpenLP.showchords) {
				$("#chords").addClass("buttonenabled")
			} else {
				$("#chords").removeClass("buttonenabled")
				$(".chord span strong").toggle()
				$(".chordline").toggleClass('nochordline')
			}
			if (bassmode) {
				$("#bassmode").addClass("buttonenabled")
			} else {
				$("#bassmode").removeClass("buttonenabled")
			}
			/*if (metronome.isRunning) {
				metronome.stop()
				metronome.start()
			}*/
			OpenLP.updateCurrentSlide(scrollToCurrentSlide)
		}
	},
	spanCount: 0,
	sheetmusicname: "",
	renderSlideHTMLElements: function() {
		lastTransposedChord = ""
		OpenLP.chordsFound = false//(OpenLP.currentSlides, OpenLP.currentSlide),
		regtempo=/Tempo=\d+/g
		regtick=/Tick=(\d+)/
		regBpB=/BpB=(\d+)/
		regswing=/Swing=?(\d+)?/
		spacer=/<span class\s*=\s*"ws">[\w\&\;\s\–]*<\/span>/g
		lastfiller = "&nbsp"
		regsheetname = /<div id="sheet" style="display:none">(.*?)<\/div>/g
		sheetmusic = function (mstr, $1) {
			if ($1) {
				if ($("#sheetbutton")[0].style.display != "inline-block") {
					OpenLP.sheetmusicname = $1
					$("#sheetbutton")[0].style.display = "inline-block"
				}
				if (OpenLP.showSheetPage) {
					if (window.innerWidth > window.innerHeight) {
						document.querySelector(':root').style.setProperty("--vw","var(--split)")
					} else {
						document.querySelector(':root').style.setProperty("--vw","1vw")
					}
					$("#kotta").show()
					OpenLP.populateKotta(OpenLP.sheetmusicname, 1)
				}
				return ""
			}
		}
		regnotes = /<span class="notes" style="display:none">(.*?)<\/span>/g
		notesReplace = function (mstr, $1) {
			return `<div class="notes">${$1}</div>`
		}
		regnochordline = /<span class="nochordline"/g
		let html = ""
		OpenLP.slideTransposeValue = 0
		for (slideCounter = 0; slideCounter < OpenLP.currentSlides.length; slideCounter++) {
			currentSlideHTML = ""
			if (debug) {console.log("Processing slide " + slideCounter)}
			lastfiller = "&nbsp"
			//if (OpenLP.currentSlides[slideCounter]["img"]) {
			if (OpenLP.currentName == "presentations") {	
				currentSlideHTML += `<span class="nochordline presentation" id="OpenLPSlide${slideCounter}"><img src="${OpenLP.currentSlides[slideCounter]["img"]}">${OpenLP.currentSlides[slideCounter]["slide_notes"]}</span>`
			/*} else if (OpenLP.currentSlides[slideCounter]["title"]) {
				currentSlideHTML += `<span class="nochordline" id="OpenLPSlide${OpenLP.currentSlide}>${OpenLP.currentSlides[slideCounter]["title"]}</span>`*/
			} else if (OpenLP.currentName == "images") {
				currentSlideHTML += `<span class="nochordline" id="OpenLPSlide${slideCounter}"><img src="${OpenLP.currentSlides[slideCounter]["img"]}"></span>`
			} else if (OpenLP.currentName == "media") {
				currentSlideHTML += `<span class="nochordline" id="OpenLPSlide${slideCounter}">${OpenLP.currentItemTitle}</span>`
			} else {
				//console.log(OpenLP.currentSlides[slideCounter]["chords_old"])
				if ((OpenLP.currentSlides[slideCounter]["chords_old"] == '<span class="nochordline"></span>') || (OpenLP.currentSlides[slideCounter]["chords_old"] == '<span class="chordline"></span>')) {
					currentSlideHTML += '<span class="nochordline emptyslide currentslide" id="OpenLPSlide0">Üres Slide</span>'
				} else {
					if (OpenLP.slideTransposeValue != OpenLP.getTransposeValue(slideCounter)) {
						if (currentChordKey) currentChordKey = transposeChord(currentChordKey, OpenLP.getTransposeValue(slideCounter) - OpenLP.slideTransposeValue, null, true)
						OpenLP.slideTransposeValue = OpenLP.getTransposeValue(slideCounter)
						if (debug) {console.log("OpenLP.slideTransposeValue = " + OpenLP.slideTransposeValue)}
						currentSlideHTML += `<div class="nexttransposevalue">Transpose${(OpenLP.slideTransposeValue <=0) ? " (capo)" : ""}: ${OpenLP.slideTransposeValue}</div>`
					}
					if (OpenLP.currentSlides[slideCounter]["chords_old"].includes("nochordline")) {
						currentSlideHTML += OpenLP.currentSlides[slideCounter]["chords_old"].replace(/font-size:.*?pt;/g,"").replace(/font-weight:.*?;/g,"").replace(/<span class="nochordline"/,`<span class="nochordline" onclick="OpenLP.showCurrentSlideSettings(${slideCounter})" id="OpenLPSlide${slideCounter}"`).replace(/<span class="ws">.*?<\/span>/g, "")
					} else {
						currentSlideHTML += OpenLP.currentSlides[slideCounter]["chords_old"].replace(/font-size:.*?pt;/g,"").replace(/font-weight:.*?;/g,"").replace(/<span class="chordline"/,`<span class="chordline" onclick="OpenLP.showCurrentSlideSettings(${slideCounter})" id="OpenLPSlide${slideCounter}"`).replace(/<span class="ws">.*?<\/span>/g, "")
					}
					//currentSlideHTML = currentSlideHTML.replace(/<div class="notes" style="display:none">/g, '<div class="notes">')
					currentSlideHTML = currentSlideHTML.replace(regnotes, notesReplace)
					//currentSlideHTML = currentSlideHTML.replace(/<nobr \/>/g, "")
					currentSlideHTML = currentSlideHTML.replace(/<span class="nobr" style="display:none">.*?<\/span>/g, "")
					if (OpenLP.showchords) {
						OpenLP.spanCount = 0; if (debug) {console.log('RESET; OpenLP.spanCount=' + OpenLP.spanCount);if (debug) {console.log("processing: " + currentSlideHTML)}}
						OpenLP.chordsFound = false
						textInSlide = false
						currentSlideHTML = currentSlideHTML.replace(/<span class="chordnewline" style="display:none"><\/span>/g,"<br>")
						currentSlideHTML = currentSlideHTML.replace(OpenLP.regchord, OpenLP.replaceChords)
						if (debug) console.log("OpenLP.chordsFound = " + OpenLP.chordsFound)
						if (debug) console.log('textInSlide='+textInSlide)
						if (!textInSlide) {currentSlideHTML = currentSlideHTML.replace(/<\/span>$/,'<span style="display:inline-block;width:.22em;"></span></span>')}
						//if (OpenLP.chordsFound) {currentSlideHTML = currentSlideHTML.replace(regnochordline, '<span class="chordline"')}
					}
					currentSlideHTML = currentSlideHTML.replace(regsheetname, sheetmusic)
				}
			}
			html += currentSlideHTML
		}
		OpenLP.metronomeList = []
		tempoAll = html.match(regtempo)
		OpenLP.metronomeCurrentIndex = null
		OpenLP.metronomeNextIndex = null
		if (tempoAll) {
			for (tempo of tempoAll) {
				OpenLP.metronomeList.push(tempo.split('=')[1])
			}
		} else {
			if (metronome.isRunning) OpenLP.metronomeStop()
		}
		if (OpenLP.metronomeList.length > 0) {
			OpenLP.metronomeCurrentIndex = 0
		}
		if (OpenLP.metronomeList.length > 1) {
			OpenLP.metronomeNextIndex = 1
		}
		OpenLP.updateMetronome()
		if (metronome.tempo >= 115) {
			metronome.skipEightNote = true
		} else {
			metronome.skipEightNote = false
		}
		beatsPerBar = html.match(regBpB)
		if (beatsPerBar && beatsPerBar[1] == '4') {
			metronome.skipEightNote = true
		}
		ticks = html.match(regtick)
		if (ticks) {
			metronome.barLength = ticks[1]
		} else {
			metronome.barLength = 4
		}
		swing = html.match(regswing)
		if (swing) {
			if (swing[1]) {
				metronome.swing = swing[1]
			} else {
				metronome.swing = 66
			}
		} else {
			metronome.swing = 50
		}
		return html
	},
	metronomeStartStop: function() {
		metronome.startStop()
		if (metronome.isRunning) {
			$("#metronome").addClass("buttonenabled")
			$("#metronomebutton").addClass("buttonenabled")
		} else {
			$("#metronome").removeClass("buttonenabled")
			$("#metronomebutton").removeClass("buttonenabled")
		}
	},
	metronomeStart: function() {
		metronome.start()
		if (metronome.isRunning) {
			$("#metronome").addClass("buttonenabled")
			$("#metronomebutton").addClass("buttonenabled")
		}
	},
	metronomeStop: function() {
		metronome.stop()
		$("#metronome").removeClass("buttonenabled")
		$("#metronomebutton").removeClass("buttonenabled")
	},
	updateMetronome: function(){
		if (OpenLP.metronomeCurrentIndex != null) {
			metronome.tempo = OpenLP.metronomeList[OpenLP.metronomeCurrentIndex]
			$("#metronomebutton").html(metronome.tempo)
		} else {
			OpenLP.metronomeStop()
			$("#metronome").removeClass("buttonenabled")
		}
		if (OpenLP.metronomeNextIndex != null) {
			$("#metronomenextbutton").html(OpenLP.metronomeList[OpenLP.metronomeNextIndex])
			$("#metronomenextbutton").show()
			$("#metronomebutton")[0].style.height = "60vh"

		} else {
			$("#metronomenextbutton").html("")
			$("#metronomenextbutton").hide()
			$("#metronomebutton")[0].style.height = "70vh"
		}
	},
	metronomeAutostartToggle: function() {
		if (OpenLP.metronomeAutostart) {
			OpenLP.metronomeAutostart = false
			$("#metronomeautostartbutton").removeClass("buttonenabled")
		} else {
			OpenLP.metronomeAutostart = true
			$("#metronomeautostartbutton").addClass("buttonenabled")
		}
	},
	setMetronomeNextTempo: function(){
		if (OpenLP.metronomeList.length > 1) {
			OpenLP.metronomeCurrentIndex = OpenLP.metronomeCurrentIndex + 1 > OpenLP.metronomeList.length - 1 ? 0 : OpenLP.metronomeCurrentIndex + 1
			OpenLP.metronomeNextIndex = OpenLP.metronomeCurrentIndex + 1 > OpenLP.metronomeList.length - 1 ? 0 : OpenLP.metronomeCurrentIndex + 1
		}
		OpenLP.updateMetronome()
	},
	metronomeCurrentIndex: 0,
	metronomeNextIndex: 0,
	metronomeList: [],
	metronomeAutostart: false,
	updateCurrentSlide: function(scrollToCurrentSlide) {
		$(".currentslide").toggleClass("currentslide")
		$(`#OpenLPSlide${OpenLP.currentSlide}`).toggleClass("currentslide")
		if (scrollToCurrentSlide) OpenLP.scrollToCurrentSlide()
		if (OpenLP.freeze == 1) OpenLP.freeze = 2
		if (OpenLP.currentName == "songs" && OpenLP.metronomeAutostart) {
			if (OpenLP.currentSlide == 0) {
				OpenLP.metronomeStart()
			}
			if (OpenLP.currentSlide == OpenLP.currentSlides.length - 1) {
				OpenLP.metronomeStop()
			}
		}
	},
	scrollToCurrentSlide: function() {
		if ($(".currentslide").length) $("#main").animate({scrollTop: $(`#OpenLPSlide${OpenLP.currentSlide}`).get(0).offsetTop - Math.min((Math.floor($("#main").height() - $(`#OpenLPSlide${OpenLP.currentSlide}`).height()) > 0 ? Math.floor($("#main").height() - $(`#OpenLPSlide${OpenLP.currentSlide}`).height()) : 0) / 4, Math.floor($("#main").height() / 5))}, 300)
	},
	slideTransposeValue: 0,
	chordsFound: false,
	regchord: /(.*?)<span class="chord"><span><strong>(=)?(.*?)<\/strong><\/span><\/span>(.*?)(?=<span class="chord"|<br>|$)(<br>)?/g,
	replaceChords: function(mstr,$1,$2,$3,$4,$5) {
		//$1 : text before chord
		//$2 : "=" for the song key setting
		//$3 : chord
		//$4 : leftover chars from the word with the chord until the next word or next chord
		//$5 : <br>
		var v='', w=''
		var $3len = 0, $4len = 0, slimchars=' fiIÍjlrt.,;/()°♭♯b#', $4true = ''
		OpenLP.chordsFound = true
		if ($2) {
			currentChordKey = transposeChord($3, OpenLP.slideTransposeValue, null, true)
			if (debug) console.log("currentChordKey = " + currentChordKey)
			//OpenLP.spanCount += ($1.match(/<span class="worshipbandwarning">/g) || []).length + ($1.match(/<span class="nochordline"/g) || []).length + ($1.match(/<span class="chordline"/g) || []).length + ($4.match(/<span class="worshipbandwarning">/g) || []).length - ($4.match(/<\/span>/g) || []).length
			OpenLP.spanCount += ($1.match(/<span/g) || []).length - ($1.match(/<\/span>/g) || []).length + ($4.match(/<span/g) || []).length - ($4.match(/<\/span>/g) || []).length
			if (debug) console.log("$1 = " + $1 + "; $4 = " + $4 + '; OpenLP.spanCount=' + OpenLP.spanCount)
			return $.grep([$1, $4, $5], Boolean).join('')
		}
		if (debug) console.log("transposeChord=" + $3 + "; newline=" + newline)
		$3 = transposeChord($3, OpenLP.slideTransposeValue, currentChordKey, false, useSpecialNotationChars)
		chordToLeft = false
		if ($3.indexOf("&gt;") > -1) {
			chordToLeft = true
			$3 = $3.replace("&gt;", "")
		}
		for (i = 0; i < $3.length; i++) $3len += slimchars.indexOf($3.charAt(i)) >= 0 ? 1 : 2
		$3temp = $3.split("/")
		if (debug) console.log(`$3temp = $3.split('/') -> $3temp=${$3temp}; $3=""`)
		$3 = ""
		for (i=0;i<$3temp.length;i++){
			if (debug) console.log(`for (i=0;i<${$3temp.length};i++) -> i=${i}`)
			$pStart = ""
			$pEnd = ""
			inChordParentheses = 0
			$chord = ""
			if (debug) console.log(`$pStart = ""; $pEnd = ""; inChordParentheses = 0; $chord = ""`)
			if (i==1) {
				$3 += "/"
				if (debug) console.log(`if (i==1) -> i=${i}; $3 += "/" -> $3 = ${$3}`)
			}
			for (j=0;j<$3temp[i].length;j++) {
				if (debug) console.log(`for (j=0;j<$3temp[i].length;j++) -> j=${j}`)
				switch ($3temp[i][j]) {
					case "(":
						if (debug) console.log(`switch ($3temp[i][j]) case "(" -> $3temp[${i}][${j}] = ${$3temp[i][j]}`)
						if (j==0) {
							$pStart = "("
							if (debug) console.log(`if (j==0) -> j=${j} -> $pStart = "("`)
						} else {
							inChordParentheses += 1
							$chord += "("
							if (debug) console.log(`else (j==0) -> j=${j} -> $chord += "(" -> $chord = ${$chord}; inChordParentheses += 1 -> inChordParentheses = ${inChordParentheses}`)
						}
						break
					case ")":
						if (debug) console.log(`switch ($3temp[i][j]) case ")" -> $3temp[${i}][${j}] = ${$3temp[i][j]}`)
						if (inChordParentheses == 0) {
							$pEnd = ")"
							if (debug) console.log(`if (inChordParentheses == 0) -> inChordParentheses=${inChordParentheses} -> $pEnd = ")"`)
						} else {
							inChordParentheses -= 1
							$chord += ")"
							if (debug) console.log(`else (inChordParentheses == 0) -> inChordParentheses=${inChordParentheses} -> inChordParentheses -= "1" -> inChordParentheses = ${inChordParentheses}; $chord += ")"`)
						}
						break
					default:
						$chord += $3temp[i][j]
						if (debug) console.log(`switch ($3temp[i][j]) default -> $3temp[${i}][${j}] = ${$3temp[i][j]} -> $chord += $3temp[i][j] -> $chord = ${$chord}`)
				}
			}
			$3 += $pStart + $chord.substr(0,1)
			if (debug) console.log(`$3 += $pStart + $chord.substr(0,1) -> $3 += ${$pStart} + ${$chord}.substr(0,1) -> $3 = ${$3}`)
			if ($chord.length > 1) {
				$3 += '<span class="chordtail">' + $chord.substr(1) + '</span>'
				if (debug) console.log(`if ($chord.length > 1) -> ${$chord.length} > 1 -> $3 += '<span class="chordtail">' + $chord.substr(1) + '</span>' -> $3 = ${$3}`)
			}
		}
		$3 += $pEnd
		if (debug) console.log(`$3 += $pEnd -> $3 += ${$pEnd} -> $3 = ${$3}`)
		if (debug) console.log("---")
		OpenLP.spanCount += ($1.match(/<span/g) || []).length - ($1.match(/<\/span>/g) || []).length + ($4.match(/<span/g) || []).length - ($4.match(/<\/span>/g) || []).length
		if (debug) console.log("$1 = " + $1 + "; $3 = " + $3 + "; $4 = " + $4 + '; OpenLP.spanCount=' + OpenLP.spanCount)
		$4true = $4.replace(/<.*?>/g,'')
		for (i = 0; i < $4true.length; i++) $4len += slimchars.indexOf($4true.charAt(i)) >= 0 ? 1 : 2
		if (debug) console.log("$3len = " + $3len + "; $4len = " + $4len)
		if (chordToLeft) {
			w = '<span style="display:inline-block;width:' + (.22 * $3len) + 'em;"></span>'
			return $.grep([$1, '<span class="chord"><span><strong>', $3, '</strong></span></span>', w, $4, $5], Boolean).join('')
		}
		if ($3len == 0) return $.grep([$1, $4, $5], Boolean).join('')
		if ($3len >= $4len) {
			if (debug) console.log("$3len=" + $3len + ">= $4len=" + $4len)
			if ($4len > 0){
				if (debug) console.log("$4len=" + $4len + "> 0")
				textInSlide = true
				if ($4.match(/\s+(?![^<>]*>)/)) {
					if (debug) console.log(`$4.match(/\s+(?![^<>]*>)/) = true`)
					//find the first space not between <> and replace with spacer
					if (debug) console.log(`$4 before replace: ${$4}`)
					$4 = $4.replace(/\s+(?![^<>]*>)/, `<span style="display:inline-block;width: ${.22 * ($3len - $4len + 3)}em;"></span>`)
					if (debug) console.log(`$4 after replace: ${$4}`)
					w = ""
					lastfiller = "&nbsp"
				} else {
					if (".,!?-".indexOf($4.slice(-1)) > -1) {
						//for (c = 0; c < $3len - $4len + 1; c++) {w += '<span style="display:inline-block;width:.22em;"></span>'}
						w = '<span style="display:inline-block;width:' + (.22 * ($3len - $4len + 1)) + 'em;"></span>'
						lastfiller = "&nbsp"
					} else {
						if (!(OpenLP.spanCount == 0 || $5)) {
							for (c = 0; c < Math.ceil(($3len - $4len) / 2) + 1; c++) {w += '_'}
							if ("aáeéiíoóöőuúüűAÁEÉIÍOÓÖŐUÚÜŰ".indexOf($4.slice(0,1)) > -1) {
								splitindex = 1
							}else {
								splitindex = 0
							}
							var s, s2
							if ($4.length > 1) {
								if (debug) console.log("$4 = "+ $4 + "; $4.length = " + $4.length + " > 1")
								s = 1
								s2 = 1
								while (s < $4.length){
									if (debug) console.log(`while s = ${s} < $4.lenght = ${$4.length} == true`)
									if (debug) console.log(`$4.slice(s=${s}) = ${$4.slice(s)}`)
									if ($4.slice(s,s+33) == '<span class="worshipbandwarning">') {
										if (debug) console.log(`$4.slice(${s},${s+33}) = ${$4.slice(s,s+33)} == '<span class="worshipbandwarning">' == true`)
										s += 33
										s2 += 33
										continue
									}
									if ($4.slice(s,s+7) == '</span>') {
										if (debug) console.log(`$4.slice(${s},${s+7}) = ${$4.slice(s,s+7)} == '</span>' == true`)
										s += 7
										s2 += 7
										continue
									}
									if ("aáeéiíoóöőuúüűAÁEÉIÍOÓÖŐUÚÜŰ".indexOf($4.slice(s,s+1)) > -1) {
										if (debug) console.log("$4.slice("+s+") = " + $4.slice(s) + " magánhangzó, break")
										break
									} else {
										if (("yY".indexOf($4.slice(s,s+1)) > -1 ) ||
											(("zZ".indexOf($4.slice(s,s+1)) > -1) && ("sS".indexOf($4.slice(s-1,s)) > -1)) ||
											(("sS".indexOf($4.slice(s,s+1)) > -1) && ("zZcC".indexOf($4.slice(s-1,s)) > -1))
										   ) {
											if (debug) console.log("$4.slice("+(s-1)+") + $4.slice("+(s)+") = " + $4.slice(s-1,s) +"+"+ $4.slice(s,s+1) + "; splitindex = " + (s-1))
											splitindex = s-1
										} else {
											if (debug) console.log("splitindex = "+s)
											splitindex = s
										}
									}
									s++
								}
							}
							if (debug) console.log(`s=${s}; s2=${s2}`)
							if (s == s2 + 1) {$4 = $4.insert(1, w)} else {$4 = $4.insert(splitindex, w)}
							w=""
							lastfiller = "_"
						}
					}
				}
			} else {
				if (!(OpenLP.spanCount == 0 || $5)) {
					if (lastfiller == "&nbsp") {
						//for (c = 0; c < $3len + 1; c++) {
						w = '<span style="display:inline-block;width:' + (.22 * ($3len + 1)) + 'em;"></span>'
					} else {
						for (c = 0; c < Math.ceil(($3len + 1) / 2); c++) {w += '_'}
					}
				}
			}
		}
		if (OpenLP.spanCount == 0 || $5) {
			newline = true
		} else {
			newline = false
		}
		return $.grep([$1, '<span class="chord"><span><strong>', $3, '</strong></span></span>', $4, w, $5], Boolean).join('')
	},
	updateClock: function(data) {
		var div = $("#clock")
		var t = new Date()
		var h = t.getHours()
		if (data.results.twelve && h > 12)
		h = h - 12
		if (h < 10) h = '0' + h + ''
		var m = t.getMinutes()
		if (m < 10)
			m = '0' + m + ''
		div.html(h + ":" + m)
	},
	processUpdate: function(state){
		OpenLP.loadService(state)
	},
	getTransposeValue: function (slideNumber) {
		while (!localStorage.getItem(OpenLP.currentItemTitle + '_transposeValue[' + slideNumber + ']') && slideNumber > 0) {
			slideNumber -= 1
		}
		if (localStorage.getItem(OpenLP.currentItemTitle + '_transposeValue[' + slideNumber + ']')) {
			return parseInt(localStorage.getItem(OpenLP.currentItemTitle + '_transposeValue[' + slideNumber + ']'))
		} else {
			return 0
		}
	},
	storeTransposeValue: function (slideNumber, transposeValue) {
		localStorage.setItem(OpenLP.currentItemTitle + '_transposeValue[' + slideNumber + ']', transposeValue)
	},
	getChordsVisibility: function () {
		if (localStorage.getItem('chordsVisibility')) {
			return localStorage.getItem('chordsVisibility') == 'false' ? false : true
		} else {
			return false
		}
	},
	getBassMode: function () {
		if (localStorage.getItem('bassMode')) {
			return localStorage.getItem('bassMode') == 'false' ? false : true
		} else {
			return false
		}
	},
	setChordsVisibility: function () {
		localStorage.setItem('chordsVisibility', OpenLP.showchords ? 'true' : 'false')
	},
	setBassMode: function() {
		localStorage.setItem('bassMode', bassmode ? 'true' : 'false')
	},
	showchords: false,
	convertHtmlToEdit: function (html) {
		var inTag = false, tags = 0, htmlNew = '', closingTag = false
		for (i=0;i<html.length;i++) {
			if (html[i] == '<') {
				inTag = true
				if (html[i+1] != "/") {tags += 1} else {closingTag = true}
				htmlNew += '<'
			}
			else if (html[i] == ">") {
				if (inTag && closingTag) {tags -= 1}
				inTag = false
				htmlNew+='>'
			}
			else if (inTag && (html[i] == '/')) {htmlNew+='/'}
			else if (tags > 0) {htmlNew += html[i]}
			else if (tags == 0) {htmlNew += '<span>' + html[i] + '</span>'}
		}
		return htmlNew
	},
	notesCountDownTimer: function () {
		e = $('#notes')
		s = OpenLP.notesTimerValue - ~~((Date.now() - OpenLP.notesTimerStartTime)/1000)
		e[0].style.color = `rgb(${Math.min(255, (300 - s) * 510 / 300)}, ${Math.min(255, s * 510 / 300)}, 0)`
		t = ""
		if (s < 0) {
			s*=-1
			t="-"
		}
		h = ~~(s/3600)
		t += (h < 10 ? "0" : "") + h + ":"
		s %= 3600
		m = ~~(s/60)
		t += (m < 10 ? "0" : "") + m + ":"
		s %= 60
		t += (s < 10 ? "0" : "") + s
		e.html(t)
	},
	notesTimerType: "single",
	notesTimerValue: 0,
	convertStringToSeconds: string => {
		seconds = 0
		numbers = string.split(":")
		for (i = numbers.length - 1; i >= 0; i--) {
			seconds += numbers[i] * (60 ** (numbers.length - 1 - i))
		}
		return seconds
	},
	manualMode: false,
	allsongs: $.ajax({url: "/api/v2/plugins/songs/search", type: "GET", contentType:"application/json", dataType:"json"}),
	showCurrentSlideSettings: function(currentSlide) {
		if (OpenLP.showchords) {
			OpenLP.currentSettingSlide = currentSlide
			$("#transposevalue").text(OpenLP.getTransposeValue(OpenLP.currentSettingSlide))
			$("#slidesettings").show()
		}
	},
	resetTranspose: function(fromslide) {
		for (i=fromslide;i<OpenLP.currentSlides.length;i++) {
			if (OpenLP.currentItemTitle + '_transposeValue[' + i + ']') localStorage.removeItem(OpenLP.currentItemTitle + '_transposeValue[' + i + ']')
		}
	},
	openSongFrom: function(source, id) {
		OpenLP.freeze = 1
		$("#liveupdatestate").text("⏸️")
		OpenLP.applyManualMode()
		if (source == "list") {
			$.ajax({url: "/api/v2/service/show", type: "POST", data: JSON.stringify({"id": id}), contentType:"application/json", dataType:"json"})
		} else {
			$.ajax({url: "/api/v2/plugins/songs/live", type: "POST", data: JSON.stringify({"id": id}), contentType:"application/json", dataType:"json"})
		}
		$("#servicelist").hide()
	},
	applyManualMode: function(){
		for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
			if (document.styleSheets[0].cssRules[i].selectorText == ".chordline span.chord span strong") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".chordline span.chord span strong { position:absolute; top:-1em; left:0; line-height:1em; font: normal 75% 'Arial Narrow', 'Avenir Next Condensed', sans-serif-condensed, Arial, sans-serif; color:yellow !important; -webkit-text-fill-color:yellow;font-weight:bold; width: max-content;}")}
			if (document.styleSheets[0].cssRules[i].selectorText == ".chordline, .nochordline") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".chordline, .nochordline {display: block; padding-top: 1em; font-size: 100%; color: white; font-weight: bold;}")}
			if (document.styleSheets[0].cssRules[i].selectorText == ".worshipbandwarning") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".worshipbandwarning {color:#f44; text-decoration:underline;}")}
		}
		$("#nextsong").hide()
	},
	applyAutoMode: function(){
		for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
			if (document.styleSheets[0].cssRules[i].selectorText == ".chordline span.chord span strong") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".chordline span.chord span strong { position:absolute; top:-1em; left:0; line-height:1em; font: normal 75% 'Arial Narrow', 'Avenir Next Condensed', sans-serif-condensed, Arial, sans-serif; color:orange !important; -webkit-text-fill-color:orange; width: max-content;}")}
			if (document.styleSheets[0].cssRules[i].selectorText == ".chordline, .nochordline") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".chordline, .nochordline {display: block; padding-top: 1em; font-size: 100%; color: gray;}")}
			if (document.styleSheets[0].cssRules[i].selectorText == ".worshipbandwarning") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".worshipbandwarning {color:#844; text-decoration:underline;}")}
		}
		$("#nextsong").show()
	},
	populateKotta(name, page) {
		url = "scores/" + name + "-" + page + ".svg"//?_=" + Date.now()
		$.ajax({
			type: 'HEAD',
			url: url,
			success: function(){
				$("#kotta").append(`<img id="kottapage${page}" src="${url+"?_=" + Date.now()}" ${page>1 ? 'class="kottaright"' : ''}/>`)
				OpenLP.populateKotta(name, page + 1)
			}
		})
	},
	showSheetPage: false,
	lastCounter: 0,
	freeze: 0,
	pollServer: function(){
		$.getJSON(
			"/api/poll",
			function (data){
				OpenLP.processUpdate(data.results)
			}
		)
	},
	showRemoteControls: localStorage.getItem("showRemoteControls") == "true" ? true : false,
	showComments: localStorage.getItem("showComments") == "true" ? true : false,
	nextServiceId: "",
	setServiceJumpId: function(id) {
		if (!OpenLP.showRemoteControls) {
			return
		}
		OpenLP.nextServiceId = id
		$(".servicelist").each(function(){
			if ($(this).attr('id') == id) {
				$(this).addClass("jumpenabled")
			} else {
				$(this).removeClass("jumpenabled")
			}
		})
	},
	selectedService: -1
}

/*! NoSleep.min.js v0.12.0 - git.io/vfn01 - Rich Tibbett - MIT license */
!function(A,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.NoSleep=e():A.NoSleep=e()}(this,(function(){return function(A){var e={};function B(g){if(e[g])return e[g].exports;var o=e[g]={i:g,l:!1,exports:{}};return A[g].call(o.exports,o,o.exports,B),o.l=!0,o.exports}return B.m=A,B.c=e,B.d=function(A,e,g){B.o(A,e)||Object.defineProperty(A,e,{enumerable:!0,get:g})},B.r=function(A){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(A,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(A,"__esModule",{value:!0})},B.t=function(A,e){if(1&e&&(A=B(A)),8&e)return A;if(4&e&&"object"==typeof A&&A&&A.__esModule)return A;var g=Object.create(null);if(B.r(g),Object.defineProperty(g,"default",{enumerable:!0,value:A}),2&e&&"string"!=typeof A)for(var o in A)B.d(g,o,function(e){return A[e]}.bind(null,o));return g},B.n=function(A){var e=A&&A.__esModule?function(){return A.default}:function(){return A};return B.d(e,"a",e),e},B.o=function(A,e){return Object.prototype.hasOwnProperty.call(A,e)},B.p="",B(B.s=0)}([function(A,e,B){"use strict";var g=function(){function A(A,e){for(var B=0;B<e.length;B++){var g=e[B];g.enumerable=g.enumerable||!1,g.configurable=!0,"value"in g&&(g.writable=!0),Object.defineProperty(A,g.key,g)}}return function(e,B,g){return B&&A(e.prototype,B),g&&A(e,g),e}}();var o=B(1),E=o.webm,n=o.mp4,C=function(){return"undefined"!=typeof navigator&&parseFloat((""+(/CPU.*OS ([0-9_]{3,4})[0-9_]{0,1}|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent)||[0,""])[1]).replace("undefined","3_2").replace("_",".").replace("_",""))<10&&!window.MSStream},Q=function(){return"wakeLock"in navigator},i=function(){function A(){var e=this;if(function(A,e){if(!(A instanceof e))throw new TypeError("Cannot call a class as a function")}(this,A),this.enabled=!1,Q()){this._wakeLock=null;var B=function(){null!==e._wakeLock&&"visible"===document.visibilityState&&e.enable()};document.addEventListener("visibilitychange",B),document.addEventListener("fullscreenchange",B)}else C()?this.noSleepTimer=null:(this.noSleepVideo=document.createElement("video"),this.noSleepVideo.setAttribute("title","No Sleep"),this.noSleepVideo.setAttribute("playsinline",""),this._addSourceToVideo(this.noSleepVideo,"webm",E),this._addSourceToVideo(this.noSleepVideo,"mp4",n),this.noSleepVideo.addEventListener("loadedmetadata",(function(){e.noSleepVideo.duration<=1?e.noSleepVideo.setAttribute("loop",""):e.noSleepVideo.addEventListener("timeupdate",(function(){e.noSleepVideo.currentTime>.5&&(e.noSleepVideo.currentTime=Math.random())}))})))}return g(A,[{key:"_addSourceToVideo",value:function(A,e,B){var g=document.createElement("source");g.src=B,g.type="video/"+e,A.appendChild(g)}},{key:"enable",value:function(){var A=this;return Q()?navigator.wakeLock.request("screen").then((function(e){A._wakeLock=e,A.enabled=!0,console.log("Wake Lock active."),A._wakeLock.addEventListener("release",(function(){console.log("Wake Lock released.")}))})).catch((function(e){throw A.enabled=!1,console.error(e.name+", "+e.message),e})):C()?(this.disable(),console.warn("\n        NoSleep enabled for older iOS devices. This can interrupt\n        active or long-running network requests from completing successfully.\n        See https://github.com/richtr/NoSleep.js/issues/15 for more details.\n      "),this.noSleepTimer=window.setInterval((function(){document.hidden||(window.location.href=window.location.href.split("#")[0],window.setTimeout(window.stop,0))}),15e3),this.enabled=!0,Promise.resolve()):this.noSleepVideo.play().then((function(e){return A.enabled=!0,e})).catch((function(e){throw A.enabled=!1,e}))}},{key:"disable",value:function(){Q()?(this._wakeLock&&this._wakeLock.release(),this._wakeLock=null):C()?this.noSleepTimer&&(console.warn("\n          NoSleep now disabled for older iOS devices.\n        "),window.clearInterval(this.noSleepTimer),this.noSleepTimer=null):this.noSleepVideo.pause(),this.enabled=!1}},{key:"isEnabled",get:function(){return this.enabled}}]),A}();A.exports=i},function(A,e,B){"use strict";A.exports={webm:"data:video/webm;base64,GkXfowEAAAAAAAAfQoaBAUL3gQFC8oEEQvOBCEKChHdlYm1Ch4EEQoWBAhhTgGcBAAAAAAAVkhFNm3RALE27i1OrhBVJqWZTrIHfTbuMU6uEFlSua1OsggEwTbuMU6uEHFO7a1OsghV17AEAAAAAAACkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVSalmAQAAAAAAAEUq17GDD0JATYCNTGF2ZjU1LjMzLjEwMFdBjUxhdmY1NS4zMy4xMDBzpJBlrrXf3DCDVB8KcgbMpcr+RImIQJBgAAAAAAAWVK5rAQAAAAAAD++uAQAAAAAAADLXgQFzxYEBnIEAIrWcg3VuZIaFVl9WUDiDgQEj44OEAmJaAOABAAAAAAAABrCBsLqBkK4BAAAAAAAPq9eBAnPFgQKcgQAitZyDdW5khohBX1ZPUkJJU4OBAuEBAAAAAAAAEZ+BArWIQOdwAAAAAABiZIEgY6JPbwIeVgF2b3JiaXMAAAAAAoC7AAAAAAAAgLUBAAAAAAC4AQN2b3JiaXMtAAAAWGlwaC5PcmcgbGliVm9yYmlzIEkgMjAxMDExMDEgKFNjaGF1ZmVudWdnZXQpAQAAABUAAABlbmNvZGVyPUxhdmM1NS41Mi4xMDIBBXZvcmJpcyVCQ1YBAEAAACRzGCpGpXMWhBAaQlAZ4xxCzmvsGUJMEYIcMkxbyyVzkCGkoEKIWyiB0JBVAABAAACHQXgUhIpBCCGEJT1YkoMnPQghhIg5eBSEaUEIIYQQQgghhBBCCCGERTlokoMnQQgdhOMwOAyD5Tj4HIRFOVgQgydB6CCED0K4moOsOQghhCQ1SFCDBjnoHITCLCiKgsQwuBaEBDUojILkMMjUgwtCiJqDSTX4GoRnQXgWhGlBCCGEJEFIkIMGQcgYhEZBWJKDBjm4FITLQagahCo5CB+EIDRkFQCQAACgoiiKoigKEBqyCgDIAAAQQFEUx3EcyZEcybEcCwgNWQUAAAEACAAAoEiKpEiO5EiSJFmSJVmSJVmS5omqLMuyLMuyLMsyEBqyCgBIAABQUQxFcRQHCA1ZBQBkAAAIoDiKpViKpWiK54iOCISGrAIAgAAABAAAEDRDUzxHlETPVFXXtm3btm3btm3btm3btm1blmUZCA1ZBQBAAAAQ0mlmqQaIMAMZBkJDVgEACAAAgBGKMMSA0JBVAABAAACAGEoOogmtOd+c46BZDppKsTkdnEi1eZKbirk555xzzsnmnDHOOeecopxZDJoJrTnnnMSgWQqaCa0555wnsXnQmiqtOeeccc7pYJwRxjnnnCateZCajbU555wFrWmOmkuxOeecSLl5UptLtTnnnHPOOeecc84555zqxekcnBPOOeecqL25lpvQxTnnnE/G6d6cEM4555xzzjnnnHPOOeecIDRkFQAABABAEIaNYdwpCNLnaCBGEWIaMulB9+gwCRqDnELq0ehopJQ6CCWVcVJKJwgNWQUAAAIAQAghhRRSSCGFFFJIIYUUYoghhhhyyimnoIJKKqmooowyyyyzzDLLLLPMOuyssw47DDHEEEMrrcRSU2011lhr7jnnmoO0VlprrbVSSimllFIKQkNWAQAgAAAEQgYZZJBRSCGFFGKIKaeccgoqqIDQkFUAACAAgAAAAABP8hzRER3RER3RER3RER3R8RzPESVREiVREi3TMjXTU0VVdWXXlnVZt31b2IVd933d933d+HVhWJZlWZZlWZZlWZZlWZZlWZYgNGQVAAACAAAghBBCSCGFFFJIKcYYc8w56CSUEAgNWQUAAAIACAAAAHAUR3EcyZEcSbIkS9IkzdIsT/M0TxM9URRF0zRV0RVdUTdtUTZl0zVdUzZdVVZtV5ZtW7Z125dl2/d93/d93/d93/d93/d9XQdCQ1YBABIAADqSIymSIimS4ziOJElAaMgqAEAGAEAAAIriKI7jOJIkSZIlaZJneZaomZrpmZ4qqkBoyCoAABAAQAAAAAAAAIqmeIqpeIqoeI7oiJJomZaoqZoryqbsuq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq7ruq4LhIasAgAkAAB0JEdyJEdSJEVSJEdygNCQVQCADACAAAAcwzEkRXIsy9I0T/M0TxM90RM901NFV3SB0JBVAAAgAIAAAAAAAAAMybAUy9EcTRIl1VItVVMt1VJF1VNVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVN0zRNEwgNWQkAkAEAkBBTLS3GmgmLJGLSaqugYwxS7KWxSCpntbfKMYUYtV4ah5RREHupJGOKQcwtpNApJq3WVEKFFKSYYyoVUg5SIDRkhQAQmgHgcBxAsixAsiwAAAAAAAAAkDQN0DwPsDQPAAAAAAAAACRNAyxPAzTPAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABA0jRA8zxA8zwAAAAAAAAA0DwP8DwR8EQRAAAAAAAAACzPAzTRAzxRBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABA0jRA8zxA8zwAAAAAAAAAsDwP8EQR0DwRAAAAAAAAACzPAzxRBDzRAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAEOAAABBgIRQasiIAiBMAcEgSJAmSBM0DSJYFTYOmwTQBkmVB06BpME0AAAAAAAAAAAAAJE2DpkHTIIoASdOgadA0iCIAAAAAAAAAAAAAkqZB06BpEEWApGnQNGgaRBEAAAAAAAAAAAAAzzQhihBFmCbAM02IIkQRpgkAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAGHAAAAgwoQwUGrIiAIgTAHA4imUBAIDjOJYFAACO41gWAABYliWKAABgWZooAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAAYcAAACDChDBQashIAiAIAcCiKZQHHsSzgOJYFJMmyAJYF0DyApgFEEQAIAAAocAAACLBBU2JxgEJDVgIAUQAABsWxLE0TRZKkaZoniiRJ0zxPFGma53meacLzPM80IYqiaJoQRVE0TZimaaoqME1VFQAAUOAAABBgg6bE4gCFhqwEAEICAByKYlma5nmeJ4qmqZokSdM8TxRF0TRNU1VJkqZ5niiKommapqqyLE3zPFEURdNUVVWFpnmeKIqiaaqq6sLzPE8URdE0VdV14XmeJ4qiaJqq6roQRVE0TdNUTVV1XSCKpmmaqqqqrgtETxRNU1Vd13WB54miaaqqq7ouEE3TVFVVdV1ZBpimaaqq68oyQFVV1XVdV5YBqqqqruu6sgxQVdd1XVmWZQCu67qyLMsCAAAOHAAAAoygk4wqi7DRhAsPQKEhKwKAKAAAwBimFFPKMCYhpBAaxiSEFEImJaXSUqogpFJSKRWEVEoqJaOUUmopVRBSKamUCkIqJZVSAADYgQMA2IGFUGjISgAgDwCAMEYpxhhzTiKkFGPOOScRUoox55yTSjHmnHPOSSkZc8w556SUzjnnnHNSSuacc845KaVzzjnnnJRSSuecc05KKSWEzkEnpZTSOeecEwAAVOAAABBgo8jmBCNBhYasBABSAQAMjmNZmuZ5omialiRpmud5niiapiZJmuZ5nieKqsnzPE8URdE0VZXneZ4oiqJpqirXFUXTNE1VVV2yLIqmaZqq6rowTdNUVdd1XZimaaqq67oubFtVVdV1ZRm2raqq6rqyDFzXdWXZloEsu67s2rIAAPAEBwCgAhtWRzgpGgssNGQlAJABAEAYg5BCCCFlEEIKIYSUUggJAAAYcAAACDChDBQashIASAUAAIyx1lprrbXWQGettdZaa62AzFprrbXWWmuttdZaa6211lJrrbXWWmuttdZaa6211lprrbXWWmuttdZaa6211lprrbXWWmuttdZaa6211lprrbXWWmstpZRSSimllFJKKaWUUkoppZRSSgUA+lU4APg/2LA6wknRWGChISsBgHAAAMAYpRhzDEIppVQIMeacdFRai7FCiDHnJKTUWmzFc85BKCGV1mIsnnMOQikpxVZjUSmEUlJKLbZYi0qho5JSSq3VWIwxqaTWWoutxmKMSSm01FqLMRYjbE2ptdhqq7EYY2sqLbQYY4zFCF9kbC2m2moNxggjWywt1VprMMYY3VuLpbaaizE++NpSLDHWXAAAd4MDAESCjTOsJJ0VjgYXGrISAAgJACAQUooxxhhzzjnnpFKMOeaccw5CCKFUijHGnHMOQgghlIwx5pxzEEIIIYRSSsaccxBCCCGEkFLqnHMQQgghhBBKKZ1zDkIIIYQQQimlgxBCCCGEEEoopaQUQgghhBBCCKmklEIIIYRSQighlZRSCCGEEEIpJaSUUgohhFJCCKGElFJKKYUQQgillJJSSimlEkoJJYQSUikppRRKCCGUUkpKKaVUSgmhhBJKKSWllFJKIYQQSikFAAAcOAAABBhBJxlVFmGjCRcegEJDVgIAZAAAkKKUUiktRYIipRikGEtGFXNQWoqocgxSzalSziDmJJaIMYSUk1Qy5hRCDELqHHVMKQYtlRhCxhik2HJLoXMOAAAAQQCAgJAAAAMEBTMAwOAA4XMQdAIERxsAgCBEZohEw0JweFAJEBFTAUBigkIuAFRYXKRdXECXAS7o4q4DIQQhCEEsDqCABByccMMTb3jCDU7QKSp1IAAAAAAADADwAACQXAAREdHMYWRobHB0eHyAhIiMkAgAAAAAABcAfAAAJCVAREQ0cxgZGhscHR4fICEiIyQBAIAAAgAAAAAggAAEBAQAAAAAAAIAAAAEBB9DtnUBAAAAAAAEPueBAKOFggAAgACjzoEAA4BwBwCdASqwAJAAAEcIhYWIhYSIAgIABhwJ7kPfbJyHvtk5D32ych77ZOQ99snIe+2TkPfbJyHvtk5D32ych77ZOQ99YAD+/6tQgKOFggADgAqjhYIAD4AOo4WCACSADqOZgQArADECAAEQEAAYABhYL/QACIBDmAYAAKOFggA6gA6jhYIAT4AOo5mBAFMAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCAGSADqOFggB6gA6jmYEAewAxAgABEBAAGAAYWC/0AAiAQ5gGAACjhYIAj4AOo5mBAKMAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCAKSADqOFggC6gA6jmYEAywAxAgABEBAAGAAYWC/0AAiAQ5gGAACjhYIAz4AOo4WCAOSADqOZgQDzADECAAEQEAAYABhYL/QACIBDmAYAAKOFggD6gA6jhYIBD4AOo5iBARsAEQIAARAQFGAAYWC/0AAiAQ5gGACjhYIBJIAOo4WCATqADqOZgQFDADECAAEQEAAYABhYL/QACIBDmAYAAKOFggFPgA6jhYIBZIAOo5mBAWsAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCAXqADqOFggGPgA6jmYEBkwAxAgABEBAAGAAYWC/0AAiAQ5gGAACjhYIBpIAOo4WCAbqADqOZgQG7ADECAAEQEAAYABhYL/QACIBDmAYAAKOFggHPgA6jmYEB4wAxAgABEBAAGAAYWC/0AAiAQ5gGAACjhYIB5IAOo4WCAfqADqOZgQILADECAAEQEAAYABhYL/QACIBDmAYAAKOFggIPgA6jhYICJIAOo5mBAjMAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCAjqADqOFggJPgA6jmYECWwAxAgABEBAAGAAYWC/0AAiAQ5gGAACjhYICZIAOo4WCAnqADqOZgQKDADECAAEQEAAYABhYL/QACIBDmAYAAKOFggKPgA6jhYICpIAOo5mBAqsAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCArqADqOFggLPgA6jmIEC0wARAgABEBAUYABhYL/QACIBDmAYAKOFggLkgA6jhYIC+oAOo5mBAvsAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCAw+ADqOZgQMjADECAAEQEAAYABhYL/QACIBDmAYAAKOFggMkgA6jhYIDOoAOo5mBA0sAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCA0+ADqOFggNkgA6jmYEDcwAxAgABEBAAGAAYWC/0AAiAQ5gGAACjhYIDeoAOo4WCA4+ADqOZgQObADECAAEQEAAYABhYL/QACIBDmAYAAKOFggOkgA6jhYIDuoAOo5mBA8MAMQIAARAQABgAGFgv9AAIgEOYBgAAo4WCA8+ADqOFggPkgA6jhYID+oAOo4WCBA+ADhxTu2sBAAAAAAAAEbuPs4EDt4r3gQHxghEr8IEK",mp4:"data:video/mp4;base64,AAAAHGZ0eXBNNFYgAAACAGlzb21pc28yYXZjMQAAAAhmcmVlAAAGF21kYXTeBAAAbGliZmFhYyAxLjI4AABCAJMgBDIARwAAArEGBf//rdxF6b3m2Ui3lizYINkj7u94MjY0IC0gY29yZSAxNDIgcjIgOTU2YzhkOCAtIEguMjY0L01QRUctNCBBVkMgY29kZWMgLSBDb3B5bGVmdCAyMDAzLTIwMTQgLSBodHRwOi8vd3d3LnZpZGVvbGFuLm9yZy94MjY0Lmh0bWwgLSBvcHRpb25zOiBjYWJhYz0wIHJlZj0zIGRlYmxvY2s9MTowOjAgYW5hbHlzZT0weDE6MHgxMTEgbWU9aGV4IHN1Ym1lPTcgcHN5PTEgcHN5X3JkPTEuMDA6MC4wMCBtaXhlZF9yZWY9MSBtZV9yYW5nZT0xNiBjaHJvbWFfbWU9MSB0cmVsbGlzPTEgOHg4ZGN0PTAgY3FtPTAgZGVhZHpvbmU9MjEsMTEgZmFzdF9wc2tpcD0xIGNocm9tYV9xcF9vZmZzZXQ9LTIgdGhyZWFkcz02IGxvb2thaGVhZF90aHJlYWRzPTEgc2xpY2VkX3RocmVhZHM9MCBucj0wIGRlY2ltYXRlPTEgaW50ZXJsYWNlZD0wIGJsdXJheV9jb21wYXQ9MCBjb25zdHJhaW5lZF9pbnRyYT0wIGJmcmFtZXM9MCB3ZWlnaHRwPTAga2V5aW50PTI1MCBrZXlpbnRfbWluPTI1IHNjZW5lY3V0PTQwIGludHJhX3JlZnJlc2g9MCByY19sb29rYWhlYWQ9NDAgcmM9Y3JmIG1idHJlZT0xIGNyZj0yMy4wIHFjb21wPTAuNjAgcXBtaW49MCBxcG1heD02OSBxcHN0ZXA9NCB2YnZfbWF4cmF0ZT03NjggdmJ2X2J1ZnNpemU9MzAwMCBjcmZfbWF4PTAuMCBuYWxfaHJkPW5vbmUgZmlsbGVyPTAgaXBfcmF0aW89MS40MCBhcT0xOjEuMDAAgAAAAFZliIQL8mKAAKvMnJycnJycnJycnXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXiEASZACGQAjgCEASZACGQAjgAAAAAdBmjgX4GSAIQBJkAIZACOAAAAAB0GaVAX4GSAhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZpgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGagC/AySEASZACGQAjgAAAAAZBmqAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZrAL8DJIQBJkAIZACOAAAAABkGa4C/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmwAvwMkhAEmQAhkAI4AAAAAGQZsgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGbQC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBm2AvwMkhAEmQAhkAI4AAAAAGQZuAL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGboC/AySEASZACGQAjgAAAAAZBm8AvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZvgL8DJIQBJkAIZACOAAAAABkGaAC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmiAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZpAL8DJIQBJkAIZACOAAAAABkGaYC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBmoAvwMkhAEmQAhkAI4AAAAAGQZqgL8DJIQBJkAIZACOAIQBJkAIZACOAAAAABkGawC/AySEASZACGQAjgAAAAAZBmuAvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZsAL8DJIQBJkAIZACOAAAAABkGbIC/AySEASZACGQAjgCEASZACGQAjgAAAAAZBm0AvwMkhAEmQAhkAI4AhAEmQAhkAI4AAAAAGQZtgL8DJIQBJkAIZACOAAAAABkGbgCvAySEASZACGQAjgCEASZACGQAjgAAAAAZBm6AnwMkhAEmQAhkAI4AhAEmQAhkAI4AhAEmQAhkAI4AhAEmQAhkAI4AAAAhubW9vdgAAAGxtdmhkAAAAAAAAAAAAAAAAAAAD6AAABDcAAQAAAQAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAzB0cmFrAAAAXHRraGQAAAADAAAAAAAAAAAAAAABAAAAAAAAA+kAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAALAAAACQAAAAAAAkZWR0cwAAABxlbHN0AAAAAAAAAAEAAAPpAAAAAAABAAAAAAKobWRpYQAAACBtZGhkAAAAAAAAAAAAAAAAAAB1MAAAdU5VxAAAAAAALWhkbHIAAAAAAAAAAHZpZGUAAAAAAAAAAAAAAABWaWRlb0hhbmRsZXIAAAACU21pbmYAAAAUdm1oZAAAAAEAAAAAAAAAAAAAACRkaW5mAAAAHGRyZWYAAAAAAAAAAQAAAAx1cmwgAAAAAQAAAhNzdGJsAAAAr3N0c2QAAAAAAAAAAQAAAJ9hdmMxAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAALAAkABIAAAASAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGP//AAAALWF2Y0MBQsAN/+EAFWdCwA3ZAsTsBEAAAPpAADqYA8UKkgEABWjLg8sgAAAAHHV1aWRraEDyXyRPxbo5pRvPAyPzAAAAAAAAABhzdHRzAAAAAAAAAAEAAAAeAAAD6QAAABRzdHNzAAAAAAAAAAEAAAABAAAAHHN0c2MAAAAAAAAAAQAAAAEAAAABAAAAAQAAAIxzdHN6AAAAAAAAAAAAAAAeAAADDwAAAAsAAAALAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAACgAAAAoAAAAKAAAAiHN0Y28AAAAAAAAAHgAAAEYAAANnAAADewAAA5gAAAO0AAADxwAAA+MAAAP2AAAEEgAABCUAAARBAAAEXQAABHAAAASMAAAEnwAABLsAAATOAAAE6gAABQYAAAUZAAAFNQAABUgAAAVkAAAFdwAABZMAAAWmAAAFwgAABd4AAAXxAAAGDQAABGh0cmFrAAAAXHRraGQAAAADAAAAAAAAAAAAAAACAAAAAAAABDcAAAAAAAAAAAAAAAEBAAAAAAEAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAkZWR0cwAAABxlbHN0AAAAAAAAAAEAAAQkAAADcAABAAAAAAPgbWRpYQAAACBtZGhkAAAAAAAAAAAAAAAAAAC7gAAAykBVxAAAAAAALWhkbHIAAAAAAAAAAHNvdW4AAAAAAAAAAAAAAABTb3VuZEhhbmRsZXIAAAADi21pbmYAAAAQc21oZAAAAAAAAAAAAAAAJGRpbmYAAAAcZHJlZgAAAAAAAAABAAAADHVybCAAAAABAAADT3N0YmwAAABnc3RzZAAAAAAAAAABAAAAV21wNGEAAAAAAAAAAQAAAAAAAAAAAAIAEAAAAAC7gAAAAAAAM2VzZHMAAAAAA4CAgCIAAgAEgICAFEAVBbjYAAu4AAAADcoFgICAAhGQBoCAgAECAAAAIHN0dHMAAAAAAAAAAgAAADIAAAQAAAAAAQAAAkAAAAFUc3RzYwAAAAAAAAAbAAAAAQAAAAEAAAABAAAAAgAAAAIAAAABAAAAAwAAAAEAAAABAAAABAAAAAIAAAABAAAABgAAAAEAAAABAAAABwAAAAIAAAABAAAACAAAAAEAAAABAAAACQAAAAIAAAABAAAACgAAAAEAAAABAAAACwAAAAIAAAABAAAADQAAAAEAAAABAAAADgAAAAIAAAABAAAADwAAAAEAAAABAAAAEAAAAAIAAAABAAAAEQAAAAEAAAABAAAAEgAAAAIAAAABAAAAFAAAAAEAAAABAAAAFQAAAAIAAAABAAAAFgAAAAEAAAABAAAAFwAAAAIAAAABAAAAGAAAAAEAAAABAAAAGQAAAAIAAAABAAAAGgAAAAEAAAABAAAAGwAAAAIAAAABAAAAHQAAAAEAAAABAAAAHgAAAAIAAAABAAAAHwAAAAQAAAABAAAA4HN0c3oAAAAAAAAAAAAAADMAAAAaAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAAAJAAAACQAAAAkAAACMc3RjbwAAAAAAAAAfAAAALAAAA1UAAANyAAADhgAAA6IAAAO+AAAD0QAAA+0AAAQAAAAEHAAABC8AAARLAAAEZwAABHoAAASWAAAEqQAABMUAAATYAAAE9AAABRAAAAUjAAAFPwAABVIAAAVuAAAFgQAABZ0AAAWwAAAFzAAABegAAAX7AAAGFwAAAGJ1ZHRhAAAAWm1ldGEAAAAAAAAAIWhkbHIAAAAAAAAAAG1kaXJhcHBsAAAAAAAAAAAAAAAALWlsc3QAAAAlqXRvbwAAAB1kYXRhAAAAAQAAAABMYXZmNTUuMzMuMTAw"}}])}));

var noSleep = new NoSleep()

document.addEventListener('click', function enableNoSleep() {
	document.removeEventListener('click', enableNoSleep, false)
	noSleep.enable()
	$("#screen").addClass("buttonenabled")
	//$("#screen").text("💡")
	/*for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
		if (document.styleSheets[0].cssRules[i].selectorText == "#screen") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule("#screen {color: rgba(0,0,0, 1);text-shadow: 0 0 0 black;}")}
	}*/
}, false)

//document.documentElement.webkitRequestFullScreen()

// Disable wake lock at some point in the future.
// (does not need to be wrapped in any user input event handler)
// noSleep.disable()

/* Open when someone clicks on the span element */
function openNav() {
	if ($("#kotta").is(":visible")) {
		$("#kotta").removeClass("kottaright")
		$("#kotta").addClass("kottafull")
		document.querySelector(':root').style.setProperty("--vw","1vw")
		$("#kotta").hide()
		OpenLP.showSheetPage = false
	} else {
		if (window.innerWidth > window.innerHeight) {
			$("#kotta").removeClass("kottafull")
			$("#kotta").addClass("kottaright")
			document.querySelector(':root').style.setProperty("--vw","var(--split)")
			$("#kottaswitchviewbutton").css("display", "inline-block")
		} else {
			$("#kotta").removeClass("kottaright")
			$("#kotta").addClass("kottafull")
			document.querySelector(':root').style.setProperty("--vw","1vw")
		}
		$("#kotta").show()
		OpenLP.showSheetPage = true
		if ($("#kottapage1").length == 0) OpenLP.populateKotta(OpenLP.sheetmusicname, 1)
	}
	OpenLP.scrollToCurrentSlide()
}

function showhidemetronome() {
	if ($("#metronomewindow").is(":visible")) {
		$("#metronomewindow").hide()
	} else {
		$("#metronomewindow").show()
	}
}

function closeNav() {
	$("#kotta").hide()
	if (window.innerWidth > window.innerHeight) {
		document.querySelector(':root').style.setProperty("--vw","1vw")
	}
	OpenLP.showSheetPage = false
	OpenLP.scrollToCurrentSlide()
}
  
function toggleNoSleep() {
	if (noSleep.enabled) {
		noSleep.disable()
		$("#screen").removeClass("buttonenabled")
		//$("#screen").text("🖥️")
		/*for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
			if (document.styleSheets[0].cssRules[i].selectorText == "#screen") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule("#screen {color: rgba(0,0,0, 0.25);text-shadow: 0 0 0 black;}")}
		}*/
	} else {
		noSleep.enable()
		$("#screen").addClass("buttonenabled")
		//$("#screen").text("💡")
		/*for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
			if (document.styleSheets[0].cssRules[i].selectorText == "#screen") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule("#screen {color: rgba(0,0,0, 1);text-shadow: 0 0 0 black;}")}
		}*/
	}
}

function toggleComments(action) {
	if (action === undefined){
		OpenLP.showComments = !OpenLP.showComments
	} else {
		OpenLP.showComments = action
	}
	updateComments()
}

function updateComments() {
	if (OpenLP.showComments) {
		$('#commentsbutton').addClass("buttonenabled")
		for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
			if (document.styleSheets[0].cssRules[i].selectorText == ".notes") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".notes {color:#3f3; font-size: .67em; font-weight: normal; height: auto; line-height:1em;}")}
		}
		localStorage.setItem("showComments", "true")
	} else {
		$("#commentsbutton").removeClass("buttonenabled")
		for (i=0;i<document.styleSheets[0].cssRules.length;i++) {
			if (document.styleSheets[0].cssRules[i].selectorText == ".notes") {document.styleSheets[0].deleteRule(i);document.styleSheets[0].insertRule(".notes {color:#3f3; font-size: .67em; font-weight: normal; height: auto; line-height:1em; display: none;}")}
		}
		localStorage.setItem("showComments", "false")
	}
}

function toggleRemoteControls(action) {
	if (action === undefined){
		OpenLP.showRemoteControls = !OpenLP.showRemoteControls
	} else {
		OpenLP.showRemoteControls = action
	}
	updateRemoteControls()
}

function updateRemoteControls() {
	if (OpenLP.showRemoteControls) {
		$("#remote").addClass("buttonenabled")
		$(".jumpbutton").each(function(){
			$(this).removeClass("hidden")
		})
		localStorage.setItem("showRemoteControls", "true")
		document.querySelector(':root').style.setProperty("--footer","27")
		$("#nextsong").children(":first").addClass("jumpenabled")
	} else {
		$("#remote").removeClass("buttonenabled")
		$(".jumpbutton").each(function(){
			$(this).addClass("hidden")
		})
		localStorage.setItem("showRemoteControls", "false")
		document.querySelector(':root').style.setProperty("--footer","0")
		$(".servicelist").each(function(){
			$(this).removeClass("jumpenabled")
		})
	}
}

function showServiceList() {
	$("#servicelist").show()
}

function toggleLiveUpdateState() {
	if (OpenLP.freeze == 2) {
		OpenLP.freeze = 0
		$("#liveupdatestate").text("▶️")
		OpenLP.applyAutoMode()
		OpenLP.pollServer()
	} else {
		OpenLP.freeze = 2
		$("#liveupdatestate").text("⏸️")
		OpenLP.applyManualMode()
	}
}

var start = null
window.addEventListener("touchstart",function(event){
	if(event.touches.length === 1){
		//just one finger touched
		start = event.touches.item(0).clientX
	}else{
		//a second finger hit the screen, abort the touch
		start = null
	}
})

window.addEventListener("touchend",function(event){
    var offset = 100 //at least 100px are a swipe
    if(start){
		//the only finger that hit the screen left it
		var end = event.changedTouches.item(0).clientX
		
		if(end > start + offset){
			//a left -> right swipe
			if ($("#kotta").is(":visible")) {
				i = 1
				while ($("#kottapage" + i).length && !$("#kottapage" + i).hasClass("kottaright")) {
					i++
				}
				if (i > 2 && $("#kottapage" + (i - 1)).length) $("#kottapage" + (i - 1)).addClass("kottaright")
			}
		}
		if(end < start - offset ){
			//a right -> left swipe
			if ($("#kotta").is(":visible")) {
				i = 1
				while ($("#kottapage" + i).length && !$("#kottapage" + i).hasClass("kottaright")) {
					i++
				}
				if ($("#kottapage" + i).length) $("#kottapage" + i).removeClass("kottaright")
			}
		}
    }
})

window.onresize = function() {
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
	OpenLP.scrollToCurrentSlide()
	if (window.innerWidth > window.innerHeight) {
//		$("#header").removeClass("header-portrait")
//		$("#header").addClass("header-landscape")
//		$("#main").removeClass("main-portrait")
//		$("#main").addClass("main-landscape")
		$("#kotta").removeClass("kottafull")
		$("#kotta").addClass("kottaright")
		if ($("#kotta").is(":visible")) {
			document.querySelector(':root').style.setProperty("--vw","var(--split)")
			$("#kottaswitchviewbutton").css("display", "inline-block")
		} else {
			document.querySelector(':root').style.setProperty("--vw","1vw")
		}		
	} else {
//		$("#header").removeClass("header-landscape")
//		$("#header").addClass("header-portrait")
//		$("#main").removeClass("main-landscape")
//		$("#main").addClass("main-portrait")
		$("#kotta").removeClass("kottaright")
		$("#kotta").addClass("kottafull")
		document.querySelector(':root').style.setProperty("--vw","1vw")
	}
}

$.ajaxSetup({ cache: false })
OpenLP.showchords = OpenLP.getChordsVisibility()
bassmode = OpenLP.getBassMode()
$("#service-prev").live("click",function () {$.ajax({url: "/api/v2/service/progress", type: "POST", data: JSON.stringify({action: "previous"}), contentType:"application/json", dataType:"json"})})
$("#service-next").live("click",function () {$.ajax({url: "/api/v2/service/progress", type: "POST", data: JSON.stringify({action: "next"}), contentType:"application/json", dataType:"json"})})
$("#slide-prev").live("click",function () {$.ajax({url: "/api/v2/controller/progress", type: "POST", data: JSON.stringify({action: "previous"}), contentType:"application/json", dataType:"json"})})
$("#slide-next").live("click",function () {$.ajax({url: "/api/v2/controller/progress", type: "POST", data: JSON.stringify({action: "next"}), contentType:"application/json", dataType:"json"})})
$("#service-jump").live("click",function () {$.ajax({url: "/api/v2/service/show", type: "POST", data: JSON.stringify({id: `${OpenLP.nextServiceId}`}), contentType:"application/json", dataType:"json"})})
$(document).ready(function() {
	//toggleNoSleep()
	updateRemoteControls()
	updateComments()
	$("#css").attr("src","stage.css?_=" + Date.now())
	$.getJSON("/api/v2/plugins/songs/search", function(data) {
		OpenLP.allsongs = data
	})
	$.getJSON("/api/v2/service/items", function(data) {
		data.forEach(element => {
			if (element.plugin == "songs") {
				$("#listsongs").append(`<div class="list" onclick='OpenLP.openSongFrom("list", "${element.id}")'>${element.title}</div>`)
			} else {
				$("#listsongs").append(`<div class="list notsong">${element.title}</div>`)
			}
		});
	})
	$("#slidesettings").click(function(e) {
		if($(e.target).is("#slidesettings")) {
			$("#slidesettings").toggle()
		}
	})
	document.onkeyup = function(e) {
		if ($("#metronomewindow").is(":visible")) {
			if (e.which == 32) {
				metronome.startStop()
			}
		}	
	}
	$("#allsongsearch").on('keyup vclick', function() {
		if ($("#allsongsearch").val() != "") {
			$("#searchsongs").show()
			$("#listsongs").hide()
		} else {
			$("#searchsongs").hide()
			$("#listsongs").show()
		}
		$("#searchsongs").empty()
		OpenLP.allsongs.forEach(element => {
			if (element[1].toLowerCase().indexOf($("#allsongsearch").val().toLowerCase()) > -1 || element[2].toLowerCase().indexOf($("#allsongsearch").val().toLowerCase()) > -1) {
				$("#searchsongs").append(`<div class="list" onclick='OpenLP.openSongFrom("search",${element[0]})'>${element[1]}</div>`)
			}
		}) 
	})
	$("#songlistclosebutton").click(function(){
		$("#servicelist").hide()
	})
	$("#songlistsearchbutton").click(function(){
		$("#searchsongs").empty()
		$("#searchsongs").show()
		$("#listsongs").hide()
		$.getJSON(`/api/v2/plugins/songs/search?text=${$("#allsongsearch").val()}`, function(data) {
			data = data.sort((a, b) => {
				if (a[1] < b[1]) {
				  return -1;
				}
			});
			data.forEach(element => {
				$("#searchsongs").append(`<div class="list" onclick='OpenLP.openSongFrom("search", ${element[0]})'>${element[1]}</div>`)
			})
		})
	})
	$("#kottaswitchviewbutton").click(function(e) {
		$("#kotta").removeClass("kottaright")
		$("#kotta").addClass("kottafull")
		document.querySelector(':root').style.setProperty("--vw","1vw")
		$("#kottaswitchviewbutton").css("display", "none")
	})
	$("#resettheresttranspose").click(function(e) {
		OpenLP.resetTranspose(OpenLP.currentSettingSlide + 1)
		OpenLP.updateSlide(false)
	})
	$("#resetalltranspose").click(function(e) {
		OpenLP.resetTranspose(0)
		OpenLP.updateSlide(false)
	})
	$('#transposeup').click(function(e) {
		$('#transposevalue').text(parseInt($('#transposevalue').text()) + 1)
		OpenLP.storeTransposeValue(OpenLP.currentSettingSlide, $('#transposevalue').text())
		OpenLP.updateSlide(false)
	})
	$('#transposedown').click(function(e) {  
		$('#transposevalue').text(parseInt($('#transposevalue').text()) - 1)
		OpenLP.storeTransposeValue(OpenLP.currentSettingSlide, $('#transposevalue').text())
		OpenLP.updateSlide(false)
	})
	$('#chords').click(function () {
		OpenLP.showchords = OpenLP.showchords ? false : true
		$("#transposebar").toggleClass("hidden")
		$("#transposebar").toggleClass("visible")
		OpenLP.updateSlide()
		OpenLP.setChordsVisibility()
	})
	$('#bassmode').click(function () {
		bassmode = !bassmode
		if (debug) console.log("bassmode = " + bassmode)
		OpenLP.updateSlide()
		OpenLP.setBassMode()
	})
	if (OpenLP.showchords) {
		$("#transposebar").toggleClass("visible")
	} else {
		$("#transposebar").toggleClass("hidden")
	}
	const host = window.location.hostname
	const websocket_port = 4317
	function wsstart(){
		ws = new WebSocket(`ws://${host}:${websocket_port}`)
		ws.onmessage = (event) => {
			const reader = new FileReader()
			reader.onload = () => {
				const state = JSON.parse(reader.result.toString()).results
				if (OpenLP.freeze < 2) {
					console.log(state)
					if ((OpenLP.counter != state.counter) || (OpenLP.service != state.service)) {
						OpenLP.counter = state.counter
						OpenLP.processUpdate(state)
					}
				}
			}
			reader.readAsText(event.data)
		}
		ws.onclose = function(){
			console.log('closed!')
			wscheck()
		}
	}
	function wscheck(){
		if(!ws || ws.readyState == 3) wsstart()
	}
	wsstart()
	setInterval(wscheck, 5000)
})